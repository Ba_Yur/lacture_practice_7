import 'package:flutter/material.dart';


class PhotoService {

  static Photo getPhoto(List<dynamic> list) {
     list.forEach((item) {
      photos.add(Photo(photoUrl: item['urls']['full'], id: item['id']));
    });
  }
}

class Photo {
  final String photoUrl;
  final String id;

  Photo({
    @required this.photoUrl,
    @required this.id,
  });
}

List<Photo> photos = [];
