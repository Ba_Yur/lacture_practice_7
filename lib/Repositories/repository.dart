import 'package:flutter/material.dart';
import 'package:lacture_practice_7/Services/proto_service.dart';

class PhotosRepo extends StatefulWidget {
  const PhotosRepo({Key key}) : super(key: key);

  @override
  _PhotosRepoState createState() => _PhotosRepoState();
}

class _PhotosRepoState extends State<PhotosRepo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      child: ListView(
        itemExtent: 200,
        children: photos.length <= 0
            ? [SizedBox()]
            : [
                ...photos.map((photo) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.network(photo.photoUrl),
                  );
                }).toList()
              ],
      ),
    );
  }
}
